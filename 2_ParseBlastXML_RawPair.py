#! /usr/bin/python3

import xml.etree.ElementTree as ET
import os
import argparse


#### Setting the ARGS parser
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                    help = 'input dir containing blast XML')
parser.add_argument('-o', '--outdir', type=str,
                    help = 'output PARENT dir contain Raw pairwise fasta')
parser.add_argument('-q', '--querydir', type=str,
                    help = 'dir contain Query sequence fasta')
parser.add_argument('-d', '--dbdir', type=str,
                    help = 'dir contain database sequence fasta')
parser.add_argument('-opt', '--headeroption', type=str,
                    help = 'seq header format, two options \
                            "full" => >sp|P01116|RASK_HUMAN GTPase KRas OS=Homo sapiens OX=9606 GN=KRAS PE=1 SV=1 \
                            "trimed" => P01116')

parser.add_argument('-l', '--log', type=str,
                    help = 'log file path')
args = parser.parse_args()

input_xml_dir = args.indir 
output_pairwise_fasta_dir = args.outdir 
query_fasta_dir = args.querydir
database_fasta_dir = args.dbdir
header_format = args.headeroption
log_file_path = args.log


###########################################################
#### Parse BLAST XML Out &&&& #############################
#### Generate Raw Paired Fasta File #######################
#### Modified  by B.W. July 2020 ##########################
###########################################################

#### Given the fasta path Return the whole sequence content
def RetrieveSeqContent(fasta_file_path):
    seq_content = ''
    with open(fasta_file_path, 'r') as f:
        seq_content += f.read()
    f.close()
    return seq_content


#### Parse the BLAST XML output
#### Return the hash table {ReferenceSeqHeader => list Candidate header}
def ObtainCandidates(BLAST_XML_Output_file):

    # initialize the query/reference sequence ID & list for hit/candidate IDs
    # Hash table (refID => List Candidates)
    reference_seq_header = ''
    list_candidates_header = []
    dic_RefSeqHeader_listCandidatesHeader = {}

    # read the XML file into a ET parser
    tree = ET.parse(BLAST_XML_Output_file)
    root = tree.getroot()

    # find hits
    for first_layer in root:
        if 'BlastOutput_query-def' == first_layer.tag:
            query_info = first_layer.text
            reference_seq_header += query_info.upper().strip()

        # find the iterations segments
        elif 'BlastOutput_iterations' == first_layer.tag:
            for second_layer in first_layer:
                if 'Iteration' == second_layer.tag:
                    for third_layer in second_layer:
                        if 'Iteration_hits' == third_layer.tag:
                            for fourth_layer in third_layer:
                                if 'Hit' == fourth_layer.tag:
                                    candidate_header = ''

                                    for fifth_layer in fourth_layer:
                                        if 'Hit_def' == fifth_layer.tag:
                                            candidate_header += fifth_layer.text
                                    list_candidates_header.append(
                                        candidate_header)

    # Push the result to the hash table
    dic_RefSeqHeader_listCandidatesHeader [reference_seq_header] =\
        list_candidates_header

    return dic_RefSeqHeader_listCandidatesHeader


def GenerateRawPairedFasta(dictionary_ref_Lhits,
                           ref_seq_folder,
                           candidate_seq_folder,
                           output_dir):
    # Invoke the hash table
    for key, value in dictionary_ref_Lhits.items():
        ref_seq_header = key
        
        ############!!!!!!!!!!!!! Options @@@@@@@@@@@@@@@@@@
        ## retreive the uniprot ID from the header UNIPROT RULE
        ## OPTION1: In trimmed uniprot ID 
        if header_format == 'trimed':
            ref_seq_ID = ref_seq_header.upper().strip()

        ## OPTION2: In full uniprot ID 
        elif header_format == 'full':
            ref_seq_ID = ref_seq_header.split("|")[1].upper().strip()


        ref_seq_PATH = ref_seq_folder + ref_seq_ID + '.fasta'
        ref_seq_content = RetrieveSeqContent(fasta_file_path=ref_seq_PATH)
        
        # print(ref_seq_ID, ref_seq_content)
        # print(value)

        for each_candidate_header in value:
            ########!!!!!!!!@@@@@@@@@@@@@@@@
            ## Condition: For PDB full name Entry 
            each_candidate_ID = each_candidate_header.strip()
            ## Complete

            ## retrieve the candidate sequence
            candidate_seq_PATH = candidate_seq_folder + \
                str(each_candidate_ID) + '.fasta'
            candidate_seq_content = RetrieveSeqContent(fasta_file_path = candidate_seq_PATH)

            # Combine two sequence content to a new fasta file
            raw_pairwise_fasta_PATH = output_dir + ref_seq_ID + '_' + each_candidate_ID + '.fasta'
            out = open(raw_pairwise_fasta_PATH, 'w')
            new_content = ref_seq_content + "\n" + candidate_seq_content
            out.write(new_content)
            out.close()
    return 0


def main(BLAST_XML_dir,
         out_parent_dir,
         query_dir,
         database_dir,
         log_path):

    log_F = open(log_path, 'w')

    ## indicative count 
    i = 0

    # BLAST all fasta in folder
    for root, dirs, files in os.walk(BLAST_XML_dir):
        for single_file in files:
            file_asb_path = os.path.join(BLAST_XML_dir, single_file)
            if single_file.endswith('.XML'):

                ## For indication of how many XML processed
                i +=1 
                out_reminder = "This is the " + str(i) + "\n"
                print(out_reminder)
                
                seqID = single_file[0:-4]
                out_son_dir = out_parent_dir + seqID + '/'
                cmd_mkdir = 'mkdir ' + out_son_dir
                os.system(cmd_mkdir)
                
                try:
                    dic_ref_listHits = ObtainCandidates(
                                       BLAST_XML_Output_file=file_asb_path)
                    GenerateRawPairedFasta(dictionary_ref_Lhits=dic_ref_listHits,
                                       ref_seq_folder=query_dir,
                                       candidate_seq_folder=database_dir,
                                       output_dir=out_son_dir)
                except:
                    out_info = single_file + "\n"
                    log_F.write(out_info)
    log_F.close()

    return 0

if __name__ == '__main__':
    main(BLAST_XML_dir= input_xml_dir,
         out_parent_dir = output_pairwise_fasta_dir ,
         query_dir = query_fasta_dir,
         database_dir = database_fasta_dir, 
         log_path = log_file_path )
