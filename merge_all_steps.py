import os 
import argparse 


#### Set args parameters
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                    help = 'input folder contains query sequences')
parser.add_argument('-b', '--blastdb', type=str,
                    help = 'input BLAST database of all PDB sequences')
parser.add_argument('-p', '--pdbdir', type=str,
                    help = 'input folder contains sequence of individual PDB chain')
parser.add_argument('-w', '--workdir', type=str,
                    help = 'working folder contains steps and final results')
parser.add_argument('-opt', '--headeroption', type=str,
                    help = 'seq header format, two options \
                            "full" => >sp|P01116|RASK_HUMAN GTPase KRas OS=Homo sapiens OX=9606 GN=KRAS PE=1 SV=1 \
                            "trimed" => P01116')
parser.add_argument('-t', '--threads', type=str,
                    help = 'assigned CPU threads')
args = parser.parse_args()

query_dir = args.indir
blast_database = args.blastdb
pdbseq_dir = args.pdbdir
working_dir = args.workdir
header_format = args.headeroption
assigned_threads_count = args.threads

## make sure has tailing slash /
query_dir = os.path.join(query_dir, '')
pdbseq_dir = os.path.join(pdbseq_dir, '')
working_dir = os.path.join(working_dir, '')


###### generate subfolders under the working dir
blastXML_path = working_dir + 'BlastXML'
blastXML_path = os.path.join(blastXML_path, '')

rawpair_path = working_dir + 'RawPair'
rawpair_path = os.path.join(rawpair_path, '')

alignedpair_path = working_dir + 'AlignedPair'
alignedpair_path = os.path.join(alignedpair_path, '')

results_path = working_dir + 'Results'
results_path = os.path.join(results_path, '')

mapping_path = working_dir + 'ResidueMapping'
mapping_path = os.path.join(mapping_path, '')

goodalignment_path = working_dir + 'selectedalignment'
goodalignment_path = os.path.join(goodalignment_path, '')

log_path = working_dir + 'LOG'
log_path = os.path.join(log_path, '')

cmd_mkdir = "mkdir" + "\t" + blastXML_path + "\t" +\
            rawpair_path + "\t" + alignedpair_path + "\t" +\
            results_path + "\t" + mapping_path + "\t" +\
            goodalignment_path + "\t" + log_path
os.system(cmd_mkdir)


#### Step 1. run BLAST 
cmd_step1 = 'python3 1_run_BLAST.py' + "\t" +\
            '-i' + "\t" + query_dir + "\t" +\
            '-o' + "\t" + blastXML_path + "\t" +\
            '-d' + "\t" + blast_database + "\t" +\
            '-t' + "\t" + assigned_threads_count


cmd_step2 =  'python3 2_ParseBlastXML_RawPair.py' + "\t"+\
              '-i' + "\t" + blastXML_path + "\t" +\
              '-o' + "\t" + rawpair_path + "\t" +\
              '-q' + "\t" + query_dir + "\t" +\
              '-d' + "\t" + pdbseq_dir + "\t" +\
              '-opt' + "\t" + header_format + "\t" +\
              '-l' + "\t" + os.path.join(log_path, 'assemble_error.log')
              

cmd_step3 =    'python3 3_runClustalW.py' + "\t" +\
               '-i' + "\t" + rawpair_path + "\t" +\
               '-o' + "\t" + alignedpair_path + "\t" +\
               '-t' + "\t" + assigned_threads_count


identity_csv_file = os.path.join(results_path, 'raw_identity.csv')
cmd_step4 = 'python3 4_CalculatePairwiseQuality.py' + "\t" +\
            '-i' + "\t" + alignedpair_path + "\t" +\
            '-o' + "\t" +  identity_csv_file  + "\t" +\
            '-l' + "\t" + os.path.join(log_path, 'identity_error_entry.log')


fully_covered_csv_file = os.path.join(results_path, 'fully_covered_unique_PDB.csv')
partial_covered_csv_file = os.path.join(results_path, 'partial_covered_nonoverlapping_PDB.csv')
cmd_step5_1 = 'python3 5_1_select_fullycoveredPDB.py' + "\t" +\
              '-i' + "\t" + identity_csv_file + "\t" +\
              '-o' + "\t" + fully_covered_csv_file
cmd_step5_2 = 'python3 5_2_select_partialcoveredPDB.py' + "\t" +\
              '-i' + "\t" + identity_csv_file + "\t" +\
              '-o' + "\t" + partial_covered_csv_file 


cmd_step6_1 = 'python3 6_map_residue_index.py' + "\t" +\
              '-i' + "\t" + fully_covered_csv_file + "\t" +\
              '-g' + "\t" + goodalignment_path + "\t" +\
              '-o' + "\t" + mapping_path + "\t" +\
              '-opt' + "\t" + header_format + "\t" +\
              '-s' + "\t" + alignedpair_path
cmd_step6_2 = 'python3 6_map_residue_index.py' + "\t" +\
              '-i' + "\t" + partial_covered_csv_file + "\t" +\
              '-g' + "\t" + goodalignment_path + "\t" +\
              '-o' + "\t" + mapping_path + "\t" +\
              '-opt' + "\t" + header_format + "\t" +\
              '-s' + "\t" + alignedpair_path

os.system(cmd_step1)
print("step1 Complete")

os.system(cmd_step2)
print("step2 Complete")

os.system(cmd_step3)
print("step3 Complete")

os.system(cmd_step4)
print("step4 Complete")

os.system(cmd_step5_1)
os.system(cmd_step5_2)
print("step5 Complete")

os.system(cmd_step6_1)
os.system(cmd_step6_2)
print("step6 Complete")