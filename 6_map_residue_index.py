import os 
import argparse 


#### Set args parameters
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--infile', type=str,
                    help = 'input identity csv file contains good PDB')
parser.add_argument('-g', '--gooddir', type=str,
                    help = 'copy the good alignment to selected folder')
parser.add_argument('-o', '--outdir', type=str,
                    help = 'output folder contains residue index mapping')
parser.add_argument('-s', '--sourcedir', type=str,
                    help = 'source dir contains raw alignment')
parser.add_argument('-opt', '--headeroption', type=str,
                    help = 'seq header format, two options \
                            "full" => >sp|P01116|RASK_HUMAN GTPase KRas OS=Homo sapiens OX=9606 GN=KRAS PE=1 SV=1 \
                            "trimed" => P01116')
args = parser.parse_args()

in_goodPDB_file = args.infile
out_folder_goodAlignment = args.gooddir
out_folder_mapping = args.outdir
header_format = args.headeroption
source_alignment_folder = args.sourcedir


def Mapping_Residue_Index(aligned_seq_pdb_fatsa_file):

    ## Retrieve useful information
    seqID = ''
    PDBID = ''

    with open(aligned_seq_pdb_fatsa_file) as f:
        all_context = f.read()
    f.close()

    list_slices = all_context.split('>')

    seq1_w_header = '>' + list_slices[1]
    seq2_w_header = '>' + list_slices[2]

    list_lines_seq1 = seq1_w_header.split("\n")
    list_lines_seq2 = seq2_w_header.split("\n")

    seq1_context = ''
    seq2_context = ''
    
    for eachline in list_lines_seq1:
        if eachline.startswith('>') == 0:
            if eachline != '':
                seq1_context += eachline
        else:
            seqID += eachline

    for eachline in list_lines_seq2:
        if eachline.startswith('>') == 0:
            if eachline != '':
                seq2_context += eachline
        else:
            PDBID += eachline

    m = len(seq1_context)

    pdb_seq_start_index = PDBID.split('_')[-3]
    pdb_seq_start_index = int(pdb_seq_start_index)
    
    ##### Options @@@@@@@@@!!!!!!!!! 
    ## option 1: header is uniprot format
    if header_format == 'full':
        seqID = seqID.split("|")[1].strip().upper()
    ## option 2: header is full name
    elif header_format == 'trimed':
        seqID = seqID[1:]

    ## PDB ids
    PDBID = PDBID[1:7]

    mapping_info_text = ''
    
    for i in range(0,m,1):
        ## Count the actual index for seq_1
        gaps_seq1_i = 0
        for j in range(0,i,1):
            if seq1_context[j] == '-':
                gaps_seq1_i += 1

        actual_seq1_i_index = 1 + i - gaps_seq1_i

        ## Count the actual index for pdb_seq(seq2)
        gaps_pdb_i = 0
        for k in range(0,i,1):
            if seq2_context[k] == '-':
                gaps_pdb_i += 1

        actual_pdb_i_index = pdb_seq_start_index + i - gaps_pdb_i

        if seq1_context[i] == seq2_context[i]:
            out_line = seqID + ',' +\
                       seq1_context[i] + ',' + str(actual_seq1_i_index) + ',' +\
                       PDBID + ',' + str(actual_pdb_i_index)
            # print(out_line)
            mapping_info_text += out_line + "\n"
    #### Information is done 

     
    ## OUTPUT: Generate output file for residue index mapping
    new_file_residue_mapping = out_folder_mapping +\
                               seqID + '_' + PDBID + '.mapping'
    out_F = open(new_file_residue_mapping, 'w')
    out_F.write(mapping_info_text)
    out_F.close()

    return 0



f = open(in_goodPDB_file, 'r')
for line in f:
    line = line.strip()

    uniprotID = line.split(',')[0]
    ##### Options @@@@@@@@@!!!!!!!!! 
    ## option 1: header is uniprot format
    if header_format == 'full':
        uniprotID = uniprotID.split("|")[1].strip().upper()
    ## option 2: header is full name
    elif header_format == 'trimed':
        uniprotID = uniprotID[1:]


    pdbID = line.split(',')[1]
    pdbID = pdbID[1:]

    original_alignment_path = source_alignment_folder +\
                            uniprotID +'/' +\
                            uniprotID + '_' + pdbID + '.fasta'
    
    cp_cmd = 'cp' + "\t" + original_alignment_path + "\t" +\
             out_folder_goodAlignment 
    try:
        os.system(cp_cmd)
    except:
        ## ignore the header combination
        pass 
f.close()


for root, subdir_list, files_list in os.walk(out_folder_goodAlignment):
    for each_file in files_list:
        file_path = os.path.join(root, each_file)
        try:
            Mapping_Residue_Index(aligned_seq_pdb_fatsa_file = file_path)
        except:
            pass