#! /usr/bin/python3
import os 
import argparse
from multiprocessing import Process, Pool


###########################################################
#### retrieve PDB seq info to fasta format ################
#### PDB seq info may differ fasta provided by RCSB #######
#### Written by B.Wang,  Feb 2018 #########################
#### Revise by B.Wang  June 2019 ##########################
###########################################################

## Set args parameters 
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                    help='input unzip PDB folder')
parser.add_argument('-o', '--outfasta', type=str,
                    help='output fasta path')
parser.add_argument('-d', '--dirindivifasta', type=str,
                    help='assigned dir contains single fasta for each individual PDB chain')
parser.add_argument('-t', '--threads', type=int,
                    help='assign CPU threads')
parser.add_argument('-l', '--log', type=str,
                    help='designate log file path')
args = parser.parse_args()

in_pdb_dir = args.indir 
out_whole_fasta_path = args.outfasta
out_individual_fasta_dir = args.dirindivifasta
out_log_path = args.log
count_threads = args.threads
logF = open(out_log_path, 'a')

def PDB2fasta(pdb_file_path):

    # dictionary Residue Tri_Letter => One Letter
    dic_tri_single = {
        ## 20 Common Amino Acids
        "ALA": "A",
        "ARG": "R",
        "ASN": "N",
        "ASP": "D",
        "CYS": "C",
        "GLN": "Q",
        "GLU": "E",
        "GLY": "G",
        "HIS": "H",
        "ILE": "I",
        "LEU": "L",
        "LYS": "K",
        "MET": "M",
        "PHE": "F",
        "PRO": "P",
        "SER": "S",
        "THR": "T",
        "TRP": "W",
        "TYR": "Y",
        "VAL": "V",

        ## Common Nucleotides convert to unknown X
        "DA": "X", 
        "DC": "X", 
        "DG": "X", 
        "DT": "X", 
        "DU": "X", 
        "DI": "X",
        "A": "X", 
        "C": "X", 
        "G": "X", 
        "U": "X", 
        "I": "X",

        ## Uncommon/Undetermined residues 
        "PYL": "O", 
        "SEC": "U",
        "UNK": "X",
        "GLX": "X",
        "ASX": "X",
        "N": "X"
    }

    try:
        # initalize list for chain, dictionary for chain => start seqnum, end seqnum 
        # dic_end ['A'] = 50 
        list_chain = []
        dic_chain_start =  {}
        dic_chain_end = {}
        PDB = ''
        
        ## obtain list of chain, PDB ID
        f = open(pdb_file_path, 'r')
        for line in f:
            if line.startswith("HEADER"):
                    PDB += line[62:66]
            if line.startswith("ATOM"):
                # residue name
                residue_tri = ""
                for i in range(17, 20, 1):
                    residue_tri += line[i]
                residue_tri = residue_tri.strip()
                residue_tri = residue_tri.upper()

                # chain identifier
                chain = line[21]

                # push unique chain to list
                if chain not in list_chain:
                    list_chain.append(chain)
        f.close()
        PDB = PDB.strip().upper()


        ## obtain dictionary for Chain => start/end seqnum 
        for each_chain in list_chain:
            list_seqnum = [] 
            f = open(pdb_file_path, 'r')
            for line in f:        
                if line.startswith("ATOM"):
                    # chain identifier
                    chain = line[21]

                    # residue sequence number
                    residue_seq_number = ""
                    for i in range(22, 26, 1):
                        residue_seq_number += line[i]
                    residue_seq_number = residue_seq_number.strip()

                    if chain == each_chain:
                        residue_seq_number = int(residue_seq_number)
                        list_seqnum.append(residue_seq_number)
            f.close()
            dic_chain_start[each_chain] = min(list_seqnum)
            dic_chain_end[each_chain] = max(list_seqnum)
        
    ################################################################
    ##### Complete chain list, diction_start/end[chain] = seqnum 
    #################################################################
        for each_chain in list_chain:

            ## the header info in for the seq, including PDB start/end seqnum  
            header = '>' + PDB + '_' + each_chain + '_' +\
                    'start_' + str(dic_chain_start[each_chain]) + '_' +\
                    'end_' + str(dic_chain_end[each_chain])
            header = header.upper()

            ## initialize the sequence string, as 'X' * length
            seq_length = dic_chain_end[each_chain] - dic_chain_start[each_chain] + 1  
            seq_string = 'X' * seq_length

            f = open(pdb_file_path, 'r')
            for line in f:
                if line.startswith("ATOM"):
                    # residue triple letter 
                    residue_tri = ""
                    for i in range(17, 20, 1):
                        residue_tri += line[i]
                    residue_tri = residue_tri.strip()
                    residue_tri = residue_tri.upper()

                    try:
                        residue_one = dic_tri_single[residue_tri]
                    except:
                        residue_one = "X"

                    # chain identifier
                    chain = line[21]

                    # residue sequence number
                    residue_seq_number = ""
                    for i in range(22, 26, 1):
                        residue_seq_number += line[i]
                    residue_seq_number = residue_seq_number.strip()

                    index_in_seq = int(residue_seq_number) - dic_chain_start[each_chain]

                    if chain == each_chain:
                        seq_string = seq_string[:index_in_seq] + residue_one + seq_string[index_in_seq+1:]
            f.close()
            entry_string = header + "\n" + seq_string + "\n"
            
            ## open the file hander for output individual fasta
            individual_fasta_file_name = header[1:] + '.fasta'
            out_individual_fasta_file = os.path.join(out_individual_fasta_dir, individual_fasta_file_name)
            outF = open(out_individual_fasta_file, 'w')
            outF.write(entry_string)
            outF.close()
            ##
    except:
        log_entry = pdb_file_path + "\t" + 'has problem' + "\n"
        logF.write(out_entry)

    return 0

def main(PDB_folder):
    ## obtain the paths for divided folder 
    list_dirs = [] 
    for root, subdirs, files in os.walk(PDB_folder):
        for subdir in subdirs:
            subdir_abs_path = os.path.join(root, subdir)
            list_dirs.append(subdir_abs_path)

    ## Collect the fasta seq info
    list_all_pdb_path = [] 

    for each_subdir in list_dirs:
        for root, dirs, files in os.walk(each_subdir):
            for each_file in files:
                file_abs_path = os.path.join(each_subdir, each_file)
                list_all_pdb_path.append(file_abs_path)

    with Pool(processes= count_threads ) as pool:
        pool.map(PDB2fasta, list_all_pdb_path)
    
    ## Once individual fasta complete, Merge them 
    merge_F = open(out_whole_fasta_path, 'w')
    
    for root, subdirs, files in os.walk(out_individual_fasta_dir):
        for each_file in files:
            if each_file.endswith('.fasta'):
                each_file_path = os.path.join(out_individual_fasta_dir, each_file)
                single_FA_content = ''
                
                tmpF = open(each_file_path, 'r')
                for line in tmpF:
                    single_FA_content += line
                tmpF.close()
                single_FA_content += "\n"

                merge_F.write(single_FA_content)
    merge_F.close()
    
    return 0 

if __name__ == "__main__":
    main(PDB_folder = in_pdb_dir)
