import os 
import argparse


#### Setting the args for customized option
parser = argparse.ArgumentParser()
parser.add_argument('-d', '--dest', type=str,
                     help='Assign the destionation folder to receive data')
parser.add_argument('-l', '--log', type=str,
                     help='Assign log file path')
args = parser.parse_args()

local_folder = args.dest
log_path = args.log

#### RCSB PORT setting, It should be constant/static
#### Do NOT change Except error 
SERVER = 'rsync.wwpdb.org::ftp'
PORT = '33444'

#### assign the rsync parameter 
rsync_cmd = 'rsync' + "\t" + '-rlpt -v -z --delete' +\
            "\t" + '--port=' + PORT + "\t" +\
            SERVER +'/data/structures/divided/pdb/' + "\t" +\
            local_folder + "\t" + ">" + log_path + "\t" +\
            '2>/dev/null'

os.system(rsync_cmd)