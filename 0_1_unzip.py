import os
import argparse 

## Set the args parameters 
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--indir', type=str,
                    help='input zip PDB folder')
parser.add_argument('-o', '--outdir', type=str,
                    help='output unzip PDB foler')
args = parser.parse_args() 

in_zip_pdb_folder = args.indir
out_unzip_pdb_folder =  args.outdir


## Copy zipped_PDB_foler to unzipped_PDB_folder 

mkdir_cmd = 'mkdir' + "\t" + out_unzip_pdb_folder 
os.system(mkdir_cmd)

cp_cmd = 'rsync -avh' + "\t" +\
          in_zip_pdb_folder + "\t" + out_unzip_pdb_folder
os.system(cp_cmd)


for root, subdirs, files in os.walk(out_unzip_pdb_folder):
    for each_subdir in subdirs:
        abs_folder_path = os.path.join(root, each_subdir)
        os.chdir(abs_folder_path)

        unzip_bash_cmd = "gzip -d *.gz"
        # print(each_subdir)
        os.system(unzip_bash_cmd)