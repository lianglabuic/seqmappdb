# Welcome to SeqMapPDB Pipeline 
## An optimal solution to retrieve 3-D structures for a query protein sequence.
### Get rid of redundant, non-maximum, low-quality PDB entries. Cheers ^-^

SeqMapPDB provides an optimal solution for retrieve representative structures and map residue indices. 

It can identify fully-covered structures if available, otherwise identify all non-overlapping partial structural domains. 

You can employ it with public experimental-determined RCSB PDB database, homology modelling database, or your in-house data. 

Overall, SeqMapPDB outperforms well-practiced SIFTS service provided by EBI, and also SeqMapPDB is definitely better than recommended PDB entries in uniprot webpage. 

SeqMapPDB follows GNU GENERAL PUBLIC LICENSE, which means for free distribution, no warranty in short.


## Download SeqMapPDB:

```
$ git clone https://lianglab-uic@bitbucket.org/lianglabuic/seqmappdb.git
```
## workflow of SeqMapPDB protocol
![88bf1296e64647ccbba25fdcf32275f7-0001(1)(1).jpg](https://bitbucket.org/repo/b5EM6Xn/images/1378795856-88bf1296e64647ccbba25fdcf32275f7-0001%281%29%281%29.jpg)



## Hardware Requirement 
In general, any modern computer can employ SeqMapPDB for proteome-scale computing. 

Solid state driver (SSD) would be the critical hardware component to accelerate process due to massive amount of IO operations. 

If enabling more CPU threads, it will take much less computation time. It takes about 134 minutes for all human proteins (~20,000 proteins) when enabling all threads of Intel i9-10850K.
 
It only consumes less than 1 GB RAM for the scenario human proteome. 

Free disk capacity should be above 500 GB if choose RCSB PDB database. 

Be Cautious on Inodes, millions small files would be generated during selecting candidates. 


## Operating System (OS) and Software Dependency 

We tested it on Ubuntu 20.04 LTS and VWware ubuntu on a host of windows 10, and expect it has flexibility to deploy on other Linux distribution (e.g. centOS, WSL distributions) if dependencies are properly installed. 

You need python3, BLAST suite, and ClustalW2.

python3 is in-built in most Linux OS. 


apt method to install BLAST suite and ClustalW2
```
$ sudo apt install ncbi-blast+
$ sudo apt install clustalw
```
As the tutorial, we set the default directory as 
```
$ /tmp
```

After pull the code from bitbucket, you should access SeqMapPDB code at
```
$ /tmp/seqmappdb
```

# Short-Cut Results
### A pre-computed mapping information for canonical form of human proteins (~5,800 proteins with PDB structures) is also deposited in the 
```
demo_data/human_mapping.tar.gz
demo_data/fully_covered_unique_PDB.csv
demo_data/partial_covered_nonoverlapping_PDB.csv
```

# 0. Preparation 
In brief, user has the query sequence as the input, and attempt to identify its 3-D structures.   

Therefore, as the preparation step, user needs to synchronize public structural database (e.g. PDB or homology modelling database), or their in-house secret structural data. 

### Reminds, synchronize PDB will take some time depending on internet speed to RCSB ftp server. It could take several hours to several days. If you cannot afford the time, you can use pre-compiled part for a short-cut/walk-through, which is at
```
demo_data/allPDB.fasta.tar.gz
demo_data/singlePDBinfasta.tar.gz
```
### A pre-compiled BLAST database contains all pdb sequence
```
demo_data/pdb.phr  
demo_data/pdb.psq
demo_data/pdb/pin
```


### 0.1.0. Synchronize the RCSB (alias: PDB) database in your local machine
```
$ mkdir /tmp/PDB
$ cd /tmp/seqmappdb
$ python3 0_0_rsync_PDB.py -d /tmp/PDB/ -l /tmp/syncPDB.log
```
### 0.1.1. Copy to another folder, and Unzip the PDB file
```
$ mkdir /tmp/unzipPDB
$ python3 0_1_unzip.py -i /tmp/PDB/ -o /tmp/unzipPDB/
```

Then, SeqMapPDB protocol will visit every .pdb//.ent (they are same format) file to read coordinate record, and obtain accurate sequence information.

### 0.1.2. Obtain PDB sequence information in fasta format
```
$ mkdir /tmp/PDBfasta
$ mkdir /tmp/PDBfasta/singlePDB
$ python3 0_2_Coordinate2fasta.py -i /tmp/unzipPDB/ -o /tmp/PDBfasta/allPDB.fasta -d /tmp/PDBfasta/singlePDB/ -t 20 -l /tmp/PDBfasta/pdb2fasta.LOG
```
### 0.1.3. Create the BLAST database for all PDB sequence information 
```
$ cd /tmp/PDBfasta
$ makeblastdb -in allPDB.fasta -dbtype prot -out pdb
```

### 0.1.4 Optional: Chop Proteome fasta file into single fasta files by every uniprot

SeqMapPDB requires each uniprot ID has its own single fasta file, ideally the header is in trimed form,.

Example: download human reference proteome 
```
$ mkdir /tmp/QuerySeq
$ cd /tmp
$ wget https://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/reference_proteomes/Eukaryota/UP000005640/UP000005640_9606.fasta.gz
$ gzip -kd UP000005640_9606.fasta.gz
```
Chop into each fasta file 
```
$ cd /tmp/seqmappdb
$ python3 0_4_seperate_uniprot.py -i /tmp/UP000005640_9606.fasta -o /tmp/QuerySeq/
```



### 0.1.5. Delete the unzip folder (which may occupy over 140 GB)
```
$ rm -rf /tmp/unzipPDB
```


# 1. Submit the jobs 

```
$  mkdir /tmp/workmapPDB
$  python3 merge_all_steps.py -i /tmp/QuerySeq -b /tmp/PDBfasta/pdb -p /tmp/PDBfasta/singlePDB/ -w /tmp/workmapPDB -opt "trimed" -t 20
```
# 2. Results

Since we set the working directory for mapping process at 
```
$ /tmp/workmapPDB
```

You can obtain the alignment quality (two-way identity, covered segments on query sequence) at 
```
$ /tmp/workmapPDB/Results/
```

Query protein sequences that have fully-covered 3-D structures
```
$ /tmp/workmapPDB/Results/fully_covered_unique_PDB.csv
```
Query protein sequences that have high-quality non-overlapping partial covered 3-D structures
```
$   /tmp/workmapPDB/Results/partial_covered_nonoverlapping_PDB.csv 
```
Redundant structure entries
```
$   /tmp/workmapPDB/Results/raw_identity.csv
```

You can obtain the residue indice mapping information at 
```
$ /tmp/workmapPDB/ResidueMapping
```

# 3. Examine Quality of PDB Structures
Since many structures may have missing atoms (at extreme case, only backbone carbons have coordinate record), it is important to check the completeness of heavy atoms in a select PDB structure. 

```
$ python3 7_Check_HeavyAtom_Completeness.py -i /tmp/seqmappdb/demo_data/low_quality_pdb/ -o low_quality_pdb.csv
```


# Citation
```
on submission
```
## Acknowledgement
```
This work is supported by NIH grants R35 GM127084 and R01 CA204962.
```

## Contact
If you meet critical bugs, please send email to 
```
bwang54@uic.edu
```



Have fun!