#! /usr/bin/python3
import argparse


#### For the fasta file containing multiple entries 
#### Seperate each single one to a single file named as Uniprot Name 

## Set args parameters 
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', type=str,
                    help='input fasta file for reference proteome sequence')
parser.add_argument('-o', '--outdir', type=str,
                    help='output directory contains seperate fasta by each chain')
args = parser.parse_args()

in_proteome_fasta = args.input 
out_fasta_path = args.outdir

def SeperateFasta(Combinated_Fasta_File, out_fasta_dir):

    ## read whole content into a string variable 
    with open(Combinated_Fasta_File) as f:
        all_context = f.read()
    f.close()
    
    ## split each sequence context 
    list_slices = all_context.split('>')
    
    ## iterate each sequence 
    for each_sequence in list_slices:
        sequence_with_header = '>' + each_sequence
        
        list_lines_seq = sequence_with_header.split("\n")

        ## to obtain header and actual sequence(Remove \n)
        uniprot_header = ''
        seq_context = ''

        for eachline in list_lines_seq:
            if eachline.startswith('>') == 0 :
                if eachline != '':
                    seq_context += eachline
            else:
                uniprot_header += eachline

        ## get uniprot id 
        try:
            uniprot_id = uniprot_header.split("|")[1].strip().upper()

            ## new file name & path 
            new_file_name = uniprot_id + '.fasta'
            new_file_path = out_fasta_dir + new_file_name

            # file content 
            new_content = '>' + uniprot_id + "\n" + seq_context
            
            # write to new file 
            out = open(new_file_path, 'w')
            out.write(new_content)
            out.close()
        except:
            print(uniprot_header)
    
    return 0

if __name__ == '__main__':
    SeperateFasta(Combinated_Fasta_File = in_proteome_fasta,
                  out_fasta_dir =  out_fasta_path )